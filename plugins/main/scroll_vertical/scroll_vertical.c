/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 1;
uint32_t options = BO_DISPLACE | BO_SCROLL | BO_VER;
char dname[] = "Scroll vertical";
char desc[] = "Scroll the screen vertically";

enum Direction { DOWNWARDS = 0, UPWARDS, RANDOM, DIRECTION_NB } Mode_e;
const char *direction_list[DIRECTION_NB] = { "Downwards", "Upwards", "Random" };
#include "scroll.h"


static Pixel_t *save = NULL;


static void
scroll_bt(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  memcpy((void *)save, (const void *)src->buffer+(BUFFSIZE-WIDTH-1)*sizeof(Pixel_t), WIDTH*sizeof(Pixel_t));
  memcpy((void *)(dst->buffer+WIDTH*sizeof(Pixel_t)), (const void *)src->buffer, (BUFFSIZE-WIDTH)*sizeof(Pixel_t));
  memcpy((void *)dst->buffer, (const void *)save, WIDTH*sizeof(Pixel_t));
}


static void
scroll_tb(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  memcpy((void *)save, (const void *)(src->buffer+WIDTH*sizeof(Pixel_t)), WIDTH*sizeof(Pixel_t));
  memcpy((void *)dst->buffer, (const void *)(src->buffer+WIDTH*sizeof(Pixel_t)), (BUFFSIZE-WIDTH)*sizeof(Pixel_t));
  memcpy((void *)(dst->buffer+(BUFFSIZE-WIDTH-1)*sizeof(Pixel_t)), (const void *)save, WIDTH*sizeof(Pixel_t));
}


static void
set_run_ptr(void)
{
  switch (direction) {
  case DOWNWARDS:
    run_ptr = &scroll_tb;
    break;

  case UPWARDS:
    run_ptr = &scroll_bt;
    break;

  case RANDOM:
  default:
    run_ptr = b_rand_boolean() ? &scroll_tb : &scroll_bt;
    break;
  }
}


int8_t
create(Context_t *ctx)
{
  save = xcalloc(WIDTH, sizeof(Pixel_t));

  return 1;
}


void
destroy(Context_t *ctx)
{
  xfree(save);
}

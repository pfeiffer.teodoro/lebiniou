# [Le Biniou](https://biniou.net)

[![pipeline status](https://gitlab.com/lebiniou/lebiniou/badges/master/pipeline.svg)](https://gitlab.com/lebiniou/lebiniou/commits/master)

# Pre-built packages

Packages exist for various GNU/Linux distributions:

 * [Debian](https://tracker.debian.org/pkg/lebiniou), [Ubuntu](https://packages.ubuntu.com/search?keywords=lebiniou&searchon=names&suite=all&section=all) as a native package
 * And for more distributions as a [flatpak](https://flatpak.org) on [Flathub](https://flathub.org/apps/details/net.biniou.LeBiniou)

# Building from sources

## GNU/Linux-based systems

### Debian-based distributions

  1. Install required dependencies

  ```sh
  sudo apt-get -qq update
  sudo apt-get -qq install autoconf pkg-config gcc make libglib2.0-dev libfftw3-dev libswscale-dev libsdl2-dev libcaca-dev libjack-dev libsndfile1-dev libmagickwand-dev libjansson-dev libulfius-dev libavcodec-dev libavformat-dev
  ```

  2. Configure, compile and install

  The configure script has several [build options](BUILD.md).

  ```sh
  ./bootstrap
  ./configure
  make
  sudo make install
  ```

  3. Follow the same steps for [lebiniou-data](https://gitlab.com/lebiniou/lebiniou-data) package, then

  4. Run

  ```sh
  lebiniou
  ```

  5. Get more options
  ```sh
  lebiniou --help
  man lebiniou
  ```

## Arch Linux

Use the PKGBUILDs from the AUR [lebiniou](https://aur.archlinux.org/packages/lebiniou)/[lebiniou-data](https://aur.archlinux.org/packages/lebiniou-data/) or build it manually:

Fetch dependencies and build tools:
```sh
pacman --needed -S base-devel fftw libmagick6 ffmpeg sdl2 libcaca
```

Build and install lebiniou-data:
```sh 
./bootstrap
./configure
make
sudo make install
```

Build and install lebiniou:
```sh
./bootstrap
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/lib/imagemagick6/pkgconfig"
./configure
make
sudo make install
```

## BSD-based systems

1. Fetch dependencies

* FreeBSD (12.0)
  ```sh
  pkg install autoconf automake pkgconf glib fftw3 ffmpeg sdl2 libcaca jackit ImageMagick
  ```

* NetBSD (8.0)
  ```sh
  pkg_add autoconf automake pkg-config glib2 fftw ffmpeg4 SDL2 libcaca jack ImageMagick
  ```

* OpenBSD (6.5)
  ```sh
  pkg_add gcc-8.3.0 glib2 fftw3 ffmpeg sdl2 libcaca jack ImageMagick
  ```

2. Configure, compile and install
  ```sh
  ./bootstrap
  ./configure
  # for OpenBSD
  CC=/usr/local/bin/egcc ./configure
  make
  make install
  ```

# Running a local build

See the [developer](https://wiki.biniou.net/doku.php?id=en:developer) section on the wiki.

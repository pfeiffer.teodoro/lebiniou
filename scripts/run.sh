#!/usr/bin/env bash
export DEV_WEB_UI=1
export ASAN_OPTIONS=detect_odr_violation=0
export LD_LIBRARY_PATH=src
make -j8 && ./src/lebiniou -b plugins $*

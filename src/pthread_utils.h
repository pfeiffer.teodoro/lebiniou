/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEBINIOU_PTHREAD_UTILS_H
#define LEBINIOU_PTHREAD_UTILS_H

#include "config.h"
#if HAVE_PTHREAD_H
#include <pthread.h>
#endif

int _xpthread_create(pthread_t *thread, const pthread_attr_t *attr,
                     void *(*start_routine) (void *), void *arg,
                     const char *file, const int line, const char *func);
int _xpthread_join(pthread_t thread, void **retval, const char *file, const int line, const char *func);
int _xpthread_mutex_lock(pthread_mutex_t *mutex, const char *file, const int line, const char *func);
int _xpthread_mutex_unlock(pthread_mutex_t *mutex, const char *file, const int line, const char *func);
int _xpthread_mutex_destroy(pthread_mutex_t *mutex, const char *file, const int line, const char *func);
int _xpthread_mutex_init(pthread_mutex_t *restrict mutex, const pthread_mutexattr_t *restrict attr,
                         const char *file, const int line, const char *func);

#define xpthread_create(thread, attr, start_routine, arg) _xpthread_create(thread, attr, start_routine, arg, __FILE__, __LINE__, __func__)
#define xpthread_join(thread, retval) _xpthread_join(thread, retval, __FILE__, __LINE__, __func__)
#define xpthread_mutex_lock(mutex) _xpthread_mutex_lock(mutex, __FILE__, __LINE__, __func__)
#define xpthread_mutex_unlock(mutex) _xpthread_mutex_unlock(mutex, __FILE__, __LINE__, __func__)
#define xpthread_mutex_destroy(mutex) _xpthread_mutex_destroy(mutex, __FILE__, __LINE__, __func__)
#define xpthread_mutex_init(mutex, attr) _xpthread_mutex_init(mutex, attr, __FILE__, __LINE__, __func__)

#endif // LEBINIOU_PTHREAD_UTILS_H

/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "globals.h"


// Use a sequence by id
int
callback_post_sequences(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;
  assert(NULL != ctx);

  const char *id_param = u_map_get(request->map_url, "id");
  if (NULL == id_param) {
    return U_CALLBACK_ERROR;
  }

  uint64_t id = xstrtoull(id_param);
  if (!SequenceManager_lock(ctx->sm)) {
    Context_set_sequence(ctx, id);
    SequenceManager_unlock(ctx->sm);
  }
  json_t *payload = json_pack("{sI}", "sequence", id);
  ulfius_set_json_body_response(response, 200, payload);
  json_decref(payload);

  return U_CALLBACK_COMPLETE;
}

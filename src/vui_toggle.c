/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"
#include "globals.h"


extern char *randomModes[BR_NB];


json_t *
vui_toggle(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  if (json_is_object(arg)) {
    const char *what = json_string_value(json_object_get(arg, "what"));
    const json_t *allow = json_object_get(arg, "allow");

    if (NULL != what) {
#ifdef DEBUG_COMMANDS
      printf(">>> VUI_TOGGLE what: %s\n", what);
#endif
      if (is_equal(what, "colormaps")) {
        if (json_boolean_value(allow)) {
          ctx->allow_auto_colormaps = !ctx->allow_auto_colormaps;
          res = json_pack("{ss sb sb}", "what", what, "value", ctx->allow_auto_colormaps, "allow", 1);
        } else {
          ctx->sm->cur->auto_colormaps = ctx->auto_colormaps = !ctx->auto_colormaps;
          Sequence_changed(ctx->sm->cur);
          Context_update_auto(ctx);
          res = json_pack("{ss sb sb}", "what", what, "value", ctx->auto_colormaps, "allow", 0);
        }
      } else if (is_equal(what, "images")) {
        if (json_boolean_value(allow)) {
          ctx->allow_auto_images = !ctx->allow_auto_images;
          res = json_pack("{ss sb sb}", "what", what, "value", ctx->allow_auto_images, "allow", 1);
        } else {
          ctx->sm->cur->auto_images = ctx->auto_images = !ctx->auto_images;
          Sequence_changed(ctx->sm->cur);
          Context_update_auto(ctx);
          res = json_pack("{ss sb sb}", "what", what, "value", ctx->auto_images, "allow", 0);
        }
      }
#ifdef WITH_WEBCAM
      else if (is_equal(what, "webcams")) {
        ctx->auto_webcams = !ctx->auto_webcams;
        res = json_pack("{ss sb}", "what", what, "value", ctx->auto_webcams);
      }
#endif
      else if (is_equal(what, "sequences")) {
        ctx->random_mode = (ctx->random_mode + 1) % BR_NB;
        if (!sequences->size) {
          if (ctx->random_mode == BR_SEQUENCES) {
            ctx->random_mode = BR_SCHEMES;
          } else if (ctx->random_mode == BR_BOTH) {
            ctx->random_mode = BR_NONE;
          }
        }
        res = json_pack("{ss ss}", "what", what, "value", randomModes[ctx->random_mode]);
      }
    }
  }

  return res;
}

/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"


#define DEBUG_BULFIUS_POST

int
callback_post_sequence(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = user_data;

  assert(NULL != ctx);
  json_error_t jerror;
  // parse request body
  json_t *body = ulfius_get_json_body_request(request, &jerror);

  if (NULL == body) {
    // error
    fprintf(stderr, "[!] %s:%d JSON error: %s\n", __FILE__, __LINE__, jerror.text);
    return U_CALLBACK_ERROR;
  }

#ifdef DEBUG_BULFIUS_POST
  DEBUG_JSON("body", body, 0);
#endif
  Sequence_t *seq = Sequence_from_json(body);
  Sequence_display(seq);

  json_t *payload = json_pack("{sI}", "uploaded", seq->id);
  ulfius_set_json_body_response(response, 200, payload);
  json_decref(payload);
#ifdef DEBUG_BULFIUS_POST
  printf("[U] POST sequence: id= %"PRIu64, seq->id);
#endif
  if (!SequenceManager_lock(ctx->sm)) {
    Sequence_copy(ctx, seq, ctx->sm->next);
    Context_set(ctx);
    SequenceManager_unlock(ctx->sm);
  }
  Sequence_delete(seq);
  json_decref(body);

  return U_CALLBACK_COMPLETE;
}

/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"


json_t *
vui_lock(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  if (json_is_string(arg)) {
    const char *what = json_string_value(arg);

    if (is_equal(what, "colormap")) {
      ctx->lock_colormap = !ctx->lock_colormap;
      res = json_pack("{ss sb}", "lock", what, "value", ctx->lock_colormap);
    } else if (is_equal(what, "image")) {
      ctx->lock_image = !ctx->lock_image;
      res = json_pack("{ss sb}", "lock", what, "value", ctx->lock_image);
    }
#ifdef WITH_WEBCAM
    else if (is_equal(what, "webcam")) {
      ctx->lock_webcam = !ctx->lock_webcam;
      res = json_pack("{ss sb}", "lock", what, "value", ctx->lock_webcam);
    }
#endif
  }
  
  return res;
}

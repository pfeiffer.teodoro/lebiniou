/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"
#include "commands.h"


Command_t *
Command_new(const enum Command_type t, const enum Command c, const json_t *a, struct _websocket_manager *e)
{
  Command_t *cmd = xcalloc(1, sizeof(Command_t));

  cmd->type = t;
  cmd->cmd = c;
  cmd->arg = json_deep_copy(a);
  cmd->emitter = e;

  return cmd;
}


void
Command_delete(Command_t *cmd)
{
  assert(NULL != cmd);
  if (NULL != cmd->arg) {
    json_decref(cmd->arg);
  }
  xfree(cmd);
}

/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "colormaps.h"


extern uint16_t http_port;


static void
Context_set_colormap(Context_t *ctx)
{
  /* have to change auto_colormaps ? */
  if (ctx->sm->next->auto_colormaps != -1) {
    /* find the cmap, if any. default cmap otherwise */
    if (NULL != ctx->cf) {
      ctx->cf->fader->target
        = (ctx->sm->next->cmap_id) ?
          Colormaps_index(ctx->sm->next->cmap_id) : 0;
      CmapFader_set(ctx->cf);
    }
    ctx->auto_colormaps = ctx->sm->next->auto_colormaps;
  } else {
#ifdef DEBUG_AUTO_MODES
    xdebug("%s: leaving auto_colormaps unchanged\n", __func__);
#endif
    ctx->sm->next->cmap_id = ctx->sm->cur->cmap_id;
  }
}


static void
Context_set_image(Context_t *ctx)
{
  /* have to change auto_images ? */
  if (ctx->sm->next->auto_images != -1) {
    /* find the image, if any. default image otherwise */
    if (NULL != ctx->imgf) {
      ctx->imgf->fader->target
        = (ctx->sm->next->image_id) ?
          Images_index(ctx->sm->next->image_id) : 0;
      ImageFader_set(ctx->imgf);
    }
    ctx->auto_images = ctx->sm->next->auto_images;
  } else {
#ifdef DEBUG_AUTO_MODES
    xdebug("%s: leaving auto_images unchanged\n", __func__);
#endif
    ctx->sm->next->image_id = ctx->sm->cur->image_id;
  }
}


void
Context_set(Context_t *ctx)
{
  GList *tmp = g_list_first(ctx->sm->cur->layers);

  /* call on_switch_off() on old plugins */
  while (NULL != tmp) {
    Layer_t *layer = tmp->data;
    Plugin_t *p = layer->plugin;

    assert(NULL != p);
    if (NULL != p->on_switch_off) {
      p->on_switch_off(ctx);
    }
    tmp = g_list_next(tmp);
  }

  Context_set_colormap(ctx);
  Context_set_image(ctx);

  /* call on_switch_on() on new plugins */
  tmp = g_list_first(ctx->sm->next->layers);
  while (NULL != tmp) {
    Layer_t *layer = (Layer_t *)tmp->data;
    Plugin_t *p = layer->plugin;

    assert(NULL != p);
    if (NULL != p->on_switch_on) {
      p->on_switch_on(ctx);
    }

    if (NULL != p->parameters) {
      /* Todo: check and use returned parameters ? */
      json_t *res = p->parameters(ctx, layer->plugin_parameters, 0);

      json_decref(res);
    }
    tmp = g_list_next(tmp);
  }

  Sequence_copy(ctx, ctx->sm->next, ctx->sm->cur);
  Params3d_from_json(&ctx->params3d, ctx->sm->cur->params3d);
  Context_update_auto(ctx);
  ctx->bandpass_min = ctx->sm->cur->bandpass_min;
  ctx->bandpass_max = ctx->sm->cur->bandpass_max;
  Sequence_display(ctx->sm->cur);

  if (http_port) {
    Context_websocket_send_sequence(ctx);
  }

  okdone("Context_set");
}
